# Basado en el ejemplo: https://www.pythontutorial.net/python-concurrency/python-threading-lock/

MAX_HILOS = 10

@contador = 0

def incrementar(num)
  10.times do
    @contador += 1
    sleep 0.1
    puts "Contador #{num}: #{@contador}"
  end
end

@hilos = []
MAX_HILOS.times do |num|
  @hilos.append Thread.new { incrementar num} 
end

@hilos.each { |hilo| hilo.join }

puts "Contador termina en: #{@contador}"
