with Ada.Text_IO;
use Ada.Text_IO;

procedure Carrera
is
    Contador : Integer := 0;
    Tarea_Numero : Integer := 1;

    task type Incrementar is
    end Incrementar;

    task body Incrementar is
        Mi_Numero : Integer;
    begin
        --  Asignar un número a esta tarea.
        Mi_Numero := Tarea_Numero;
        Tarea_Numero := Tarea_Numero + 1;

        for I in 1 .. 10 loop
            Contador := Contador + 1;
            delay 0.1;
            Put_Line ("Contador "
                & Mi_Numero'Image
                & ": "
                & Contador'Image);
        end loop;
    end Incrementar;

    Max_Tareas : constant Integer := 10;

    Tareas : array (1 .. Max_Tareas) of Incrementar;

begin
    Put_Line ("Tarea principal terminada.");
end Carrera;
