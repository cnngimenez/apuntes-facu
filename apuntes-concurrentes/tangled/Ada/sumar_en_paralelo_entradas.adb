with Ada.Text_IO;
use Ada.Text_IO;

procedure Sumar_En_Paralelo_Entradas
is
    Num : Integer := 0;

    task type Sumar is
        entry Iniciar (Ident, Cantidad : Integer);
    end Sumar;

    task body Sumar is
        Id, Incremento : Integer;
    begin
        accept Iniciar (Ident, Cantidad : Integer) do
            Id := Ident;
            Incremento := Cantidad;
        end Iniciar;

        for i in 1 .. 25 loop
            Num := Num + Incremento;
            Put ("Sumar " & Id'Image & ": ");
            Put_Line ("Num contiene: " & Num'Image);
            delay 0.5;
        end loop;
    end Sumar;

    Sumar_Uno, Sumar_Dos : Sumar;
begin
    Put_Line ("Hilo principal.");
    Put_Line ("Num contiene: " & Num'Image);

    delay 1.0;

    Sumar_Uno.Iniciar (1, 1);
    Sumar_Dos.Iniciar (2, 2);

end Sumar_En_Paralelo_Entradas;
