with Ada.Text_IO;
use Ada.Text_IO;

procedure Sumar_En_Paralelo 
is
    Num : Integer := 0;

    task Sumar_Uno;
    task Sumar_Dos;

    task body Sumar_Dos is
    begin
        for i in 1 .. 25 loop
            Num := Num + 2;
            Put ("Sumar_Dos: ");
            Put_Line ("Num contiene: " & Num'Image);
            delay 0.5;
        end loop;
    end Sumar_Dos;

    task body Sumar_Uno is
    begin
        for i in 1 .. 25 loop
            Num := Num + 1;
            Put ("Sumar_Uno: ");
            Put_Line ("Num contiene: " & Num'Image);
            delay 0.5;
        end loop;
    end Sumar_Uno;    
begin
    Put_Line ("Tarea principal.");
    Put_Line ("Num contiene: " & Num'Image);    
end Sumar_En_Paralelo;
