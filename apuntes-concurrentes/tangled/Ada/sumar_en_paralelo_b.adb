with Ada.Text_IO;
use Ada.Text_IO;

procedure Sumar_En_Paralelo_B
is
    Num : Integer := 0;
    Id : Integer := 1;

    task type Sumar;

    task body Sumar is
        Mi_Id : integer := Id;
    begin
        Id := Id + 1;

        for i in 1 .. 25 loop
            Num := Num + Mi_Id;
            Put ("Sumar " & Mi_Id'Image & ": ");
            Put_Line ("Num contiene: " & Num'Image);
            delay 0.5;
        end loop;
    end Sumar;

    Sumar_Uno, Sumar_Dos : Sumar;
begin
    Put_Line ("Hilo principal.");
    Put_Line ("Num contiene: " & Num'Image);
end Sumar_En_Paralelo_B;
