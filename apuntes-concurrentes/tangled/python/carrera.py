# Basado en el ejemplo: https://www.pythontutorial.net/python-concurrency/python-threading-lock/

from threading import Thread
from time import sleep

MAX_HILOS = 10

contador = 0  # Variable compartida

def incrementar():
    global contador

    for i in range(10):
        contador += 1
        sleep(0.1)
        print(f'Contador: {contador}')

hilos = [Thread(target=incrementar) for i in range(MAX_HILOS)]
for h in hilos:
    h.start()
for h in hilos:
    h.join()

print(f'Contador terminó con: {contador}')
