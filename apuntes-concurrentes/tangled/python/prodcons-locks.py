from threading import Thread, Lock
from time import sleep

class Buffer:
    def __init__(self, max = 5):
        self.max = 5
        self.lst_productos = []

    def length(self):
        return len(self.lst_productos)

    def insertar(self, producto):
        if len(self.lst_productos) == self.max:
            raise Exception('No se puede agregar cuando el buffer está lleno')
       
        self.lst_productos.append(producto)

    def quitar(self):
        if len(self.lst_productos) == 0:
            raise Exception('No se puede quitar cuando el buffer está vacío')
        
        return self.lst_productos.pop()

class Productor:
    def __init__(self, nombre, buffer, mutexs, cant = 10):
        self.buffer = buffer
        self.nombre = nombre
        self.mutexs = mutexs
        self.cant = cant

    def iniciar(self):
        while self.cant > 0:
            self.producir()

    def producir(self):
        self.mutexs['prods'].acquire()
        producto = self.nombre + " produjo el número " + str(self.cant)
        self.buffer.insertar(producto)
        print(producto)
        self.mutexs['cons'].release()

        self.cant -= 1

class Consumidor:
    def __init__(self, nombre, buffer, mutexs, cant = 20):
        self.nombre = nombre
        self.mutexs = mutexs
        self.buffer = buffer
        self.cant = cant

    def iniciar(self):
        while self.cant > 0:
            self.consumir()

    def consumir(self):
        self.mutexs['cons'].acquire()
        producto = self.buffer.quitar()        
        print(self.nombre + " consume: " + str(producto))
        self.mutexs['prods'].release()

        self.cant -= 1

buffer = Buffer()
mutexs = {
    'prods': Lock(),
    'cons': Lock()
}
mutexs['cons'].acquire(blocking=False)

consumidores = [
    Consumidor('Cons1', buffer, mutexs)
]

productores = [
    Productor('Prod1', buffer, mutexs),
    Productor('Prod2', buffer, mutexs)
]

hilos = []

for cons in consumidores:
    hilos.append(Thread(target=cons.iniciar))

for prod in productores:
    hilos.append(Thread(target=prod.iniciar))


for h in hilos:
    h.start()

for h in hilos:
    h.join()
