frases = []

# Una persona que le dice palabras al aire.
class Persona
  PALABRAS_COMUNES = [
    'hola', 'mundo', 'estoy', 'probando', 'decir',
    'palabras', 'aleatorias', 'para', 'jugar', 'con',
    'el', 'loro'
                     ]
  def initialize(nombre, frases, cant=50)
    @nombre = nombre
    @frases = frases
    @cant = cant
    @palabras = PALABRAS_COMUNES
  end

  def iniciar
    until @cant.zero?
      decir_frase      
    end
  end

  def decir_frase
    palabra = @palabras[Random.rand(@palabras.length)]
    palabra = "#{@frases.length} #{palabra}"
    
    puts "#{@nombre}: #{palabra}"
    @frases.append palabra
    @cant -= 1
  end
end

# Loro que repite las frases que hay en el ambiente.
#
# Cada loro repite hasta 100 frases, porque después se cansa.
class Loro
  def initialize(nombre, frases)
    @frases = frases
    @nombre = nombre
    @cant = 100
  end

  def iniciar
    until @cant.zero?
      repetir(@frases.pop) unless @frases.empty?
    end
  end

  def repetir(frase)
    puts "#{@nombre}: #{frase}"
    @cant -= 1
  end

  def cansado?
    @cant.zero?
  end
end

personas = [
  Persona.new('Alicia', frases),
  Persona.new('Bob', frases)
]

loros = [
  Loro.new('Pepe', frases)
]

personas.each do |persona|
  Thread.new { persona.iniciar }
end

loros.each do |loro|
  Thread.new { loro.iniciar }
end

sleep 1 until loros.all? { |loro| loro.cansado? }
