# Buffer de productos con capacidad limitada.
class Buffer
  def initialize(max = 5)
    @max = max
    @lst_productos = []
  end

  # Cantidad máxima de productos que se pueden guardar.
  attr_reader :max

  # Retorna la cantidad de productos almacenados
  def length    
    @lst_productos.length
  end

  # Insertar un producto en el buffer.
  #
  # Si el buffer está lleno, emitir excepción.
  def insertar(producto)
    raise 'No se puede agregar producto! Buffer lleno' if @lst_productos.length == @max

    @lst_productos.append producto
  end

  # Quitar un producto del buffer.
  #
  # Si el buffer está vacío, emitir excepción.
  def quitar
    raise 'No se puede quitar producto! Buffer vacío!' if @lst_productos.empty?

    @lst_productos.pop
  end
end

class Productor
  def initialize(nombre, buffer, cant = 10)
    @buffer = buffer
    @nombre = nombre
    @cant = cant
  end

  # Inicia la producción.
  def iniciar
    until @cant.zero?
      producir
    end
  end

  # Producir un producto.
  def producir
    producto = "#{@nombre} produjo el número #{@cant}"
    @buffer.insertar producto
    puts producto

    @cant -= 1
  end
end

class Consumidor
  def initialize(nombre, buffer, cant = 20)
    @nombre = nombre
    @buffer = buffer
    @cant = cant
  end

  def iniciar
    until @cant.zero?
      consumir # unless @buffer.empty?
    end
  end

  def consumir
    producto = @buffer.quitar
    puts "#{@nombre} consume: #{producto}"  

    @cant -= 1
  end
end

buffer = Buffer.new

# Se espera un consumidor únicamente.
consumidores = [
  Consumidor.new('Cons1', buffer)
]

productores = [
  Productor.new('Prod1', buffer),
  Productor.new('Prod2', buffer)
]

hilos = []

consumidores.each do |consumidor|
  hilos.append Thread.new { consumidor.iniciar }
end

productores.each do |productor|
  hilos.append Thread.new { productor.iniciar }
end

hilos.each {|hilo| hilo.join }

puts "Terminado, restan consumir: #{buffer.length} productos."
