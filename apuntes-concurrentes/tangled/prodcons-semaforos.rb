require 'concurrent'

class Buffer
  def initialize(max = 5)
    @max = max
    @lst_productos = []
  end

  attr_reader :max

  def length
    @lst_productos.length
  end

  def insertar(producto)
    raise 'No se puede agregar producto! Buffer lleno' if @lst_productos.length == @max

    @lst_productos.append producto
  end

  def quitar
    raise 'No se puede quitar producto! Buffer vacío!' if @lst_productos.empty?

    @lst_productos.pop
  end
end

class Productor
  def initialize(nombre, buffer, mutexs, cant = 10)
    @buffer = buffer
    @mutexs = mutexs
    @nombre = nombre
    @cant = cant
  end

  def iniciar
    until @cant.zero?
      producir
    end
  end

def producir
    @mutexs[:prods].acquire
    producto = "#{@nombre} produjo el número #{@cant}"
    @buffer.insertar producto
    puts producto
    @mutexs[:cons].release

    @cant -= 1
  end
end

class Consumidor
  def initialize(nombre, buffer, mutexs, cant = 20)
    @nombre = nombre
    @mutexs = mutexs
    @buffer = buffer
    @cant = cant
  end

  def iniciar
    until @cant.zero?
      consumir # unless @buffer.empty?
    end
  end

  def consumir
    @mutexs[:cons].acquire
    producto = @buffer.quitar
    puts "#{@nombre} consume: #{producto}"  
    @mutexs[:prods].release
    
    @cant -= 1
  end
end

buffer = Buffer.new

# semáforos:
mutexs = {
  # Exclusión mutua entre productores
  prods: Concurrent::Semaphore.new(buffer.max),
  # Exclusión mutua entre productores y el consumidor
  cons: Concurrent::Semaphore.new(0)
} 

# Se espera un consumidor únicamente.
consumidores = [
  Consumidor.new('Cons1', buffer, mutexs)
]

productores = [
  Productor.new('Prod1', buffer, mutexs),
  Productor.new('Prod2', buffer, mutexs)
]

hilos = []

consumidores.each do |consumidor|
  hilos.append Thread.new { consumidor.iniciar }
end

productores.each do |productor|
  hilos.append Thread.new { productor.iniciar }
end

hilos.each {|hilo| hilo.join }

puts "Terminado, restan consumir: #{buffer.length} productos."
