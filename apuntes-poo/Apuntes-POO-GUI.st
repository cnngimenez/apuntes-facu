m1 := Morph new color: Color blue.
m1 width: 100.
m1 height: 100.
m1 openInWorld .

m2 := Morph new color: Color red.
m2 openInWorld .

"Para borrar un Morph:"
m2 delete.

"lbl := 'Hola' asMorph."
lbl := LabelMorph newLabel: 'Hola'.
m1 addMorph: lbl.
lbl position: m1 position.
lbl backgroundColor: Color gray.
lbl color: Color white.
lbl top: (lbl top + 1).
lbl left: (lbl left + 1).

btn := SimpleButtonMorph newWithLabel: 'Click me'.
m1 addMorph: btn.
btn position: m1 position.
btn top: (btn top + 20).
btn left: (btn left + 10).

btn target: [ :btn :lbl| "argumentos: btn lbl"
	Transcript show: 'hello'. 	
	lbl contents: 'Clicked!'.
	btn label: 'Clicked!'.
	].

btn actionSelector: #value:value:.

args := Array new: 2.
args at: 1 put: btn.
args at: 2 put: lbl.

btn arguments: args.

"Para actualizar un label:"
lbl backgroundColor: Color red.
lbl hide.
lbl show.

SimpleButtonMorph subclass: #MyButton
  instanceVariableNames: ''
  classVariableNames: ''
  poolDictionaries: ''
  category: 'game'.

MyButton >> #initialize
  super initialize.
  self label: 'mybutton'.
  self on: #click send: #value to: (self label: 'clicked!').

MyButton class >> #newWith: aMorph

  | btn | 
  btn := self new.
  btn label: 'mybutton2'.
  btn on: #click send: #value:value:value:
                 to: [ :lbl :evt :me_btn |
                       lbl contents: 'clicked2!' ]
                 withValue: aMorph.
  ^btn

mb := MyButton new.
m1 addMorph: mb.
mb position: m1 position.
mb left: (mb left + 10).
mb top: (mb top + 45).

mb2 := MyButton newWidth: lbl.
m1 addMorph: mb2.
mb2 position: m1 position.
mb2 left: (mb2 left + 10).
mb2 top: (mb2 top + 75).
